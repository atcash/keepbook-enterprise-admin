import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-entity-sidebar',
  templateUrl: './entity-sidebar.component.html',
  styleUrls: ['./entity-sidebar.component.css']
})
export class EntitySidebarComponent implements OnInit {
  isActive=1;
  constructor() { }

  ngOnInit() {
  }


  setSidebarActive(value){
    this.isActive=value;
  }
}
